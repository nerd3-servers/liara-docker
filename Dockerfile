# Alpine with Git
FROM alpine:latest as liara
RUN apk --no-cache add ca-certificates git

# Clone latest custom fork of Liara
WORKDIR /app
RUN git clone https://github.com/nerdcubed-community/Liara.git

# Python Image
FROM python:3.6-alpine
WORKDIR /app

# Copy Liara Source
COPY --from=liara /app/Liara .

# Copy custom pip modules
COPY ./pip/requirements.txt ./custom.txt

# Install Requirements
RUN apk add --no-cache tini \
    build-base \
    zlib-dev \
    jpeg-dev && \
  pip install -r custom.txt && \
  apk del build-base

# Mount Custom Cogs
RUN mkdir /app/cogs/bot
VOLUME ["/app/cogs/bot"]

# Run
ENTRYPOINT ["/sbin/tini", "--"]
CMD ["python", "-u", "./liara.py"]
