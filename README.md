# Liara Docker
Docker Compose instructions for deploying Discord bots based on the Liara framework. Comes bundled with some base management cogs.

## Usage
### Prerequisites
- [Docker](https://docs.docker.com/engine/installation/)
- [Docker Compose](https://docs.docker.com/compose/install/)

### `.env` File
Liara supports environment variables as config, so copy `example.env` to `bot.env` and fill in the details.

### Image
If you want to update the image, do `docker-compose pull`.  
Docker will pull a fresh image automatically on first run if you don't have one saved locally.

### Setting up your own repository
We strongly recommend cloning this repository into your own repository, and modifying the `docker-compose.yml` to point to your own repository's registry.  
For more information on how to do this, visit your repository's Registry tab.  
GitHub is not currently supported by us.

### Running the Bot
Start the service using `docker-compose up`  
This will start a Redis server and the Liara instance. It starts running in the foreground. `CTRL+C` to stop. You can also run it in detatched mode with `docker-compose up -d`.  
When you're done, `docker-compose rm` to remove the leftover containers.

Anything in the `bot` folder gets linked into the `cogs/bot` folder.  
Use `pip/requirements.txt` to add custom python modules. These get installed with a new version of the container and require it to be restarted.

We use `yapf` to enforce Google's style guide, and builds will fail if cogs/utils fail to fully comply with it.   
To install `yapf`, simply run `pip install yapf`. Then, you can run `yapf --style google -r -i bot` to automatically format your cogs.

On first boot your prefix will be randomised. To see what your prefix is, check the bot's logs with `docker-compose logs -f`. Use `[p]set prefix` to change it.

### Setting up Cogs
In Discord, run the command `[p]load cogs.bot.manage`, and then run `[p]loadall` to load all the cogs in the `bot` folder.

Custom utilities/scripts can be placed in the `bots/utils` folder.

#### `manage.py`
This cog manages the loading and unloading of cogs, and replaces Liara's load/unload commands. This cog includes some commands to make loading cogs from the `bot` folder easier.

- `[p]bl <cog>` - Loads the specified cog from the `bot` folder
- `[p]bu <cog>` - Unloads the specified cog from the `bot` folder
- `[p]br <cog>` - Reloads the specified cog from the `bot` folder

With the standard `load`, `unload` and `reload` commands, you would need to specify the cog's full path. For example: `[p]load cogs.bot.help`, so these new commands save a bit of time.

#### `errors.py`
This is a custom error handler written for the bot, which overrides Liara's error handler. By default, it doesn't return any message on the following error types:

- `CommandNotFound`
- `CheckFailure`
- `DisabledCommand`
- `NoPrivateMessage`

The cog can be modified to return messages on these errors if you wish. You can also return a link to an issue tracker when a command errors by editing the `/config/errors.json` file.

#### `help.py`
This is a custom-made help cog, which uses embeds and supports some extra features. You can modify the text and images shown under `[p]help` by editing the `/config/help.json` file with your own description and whatever.

Commands are grouped based on the group defined in the cog, and you can include a custom icon to represent your cog which appears when someone runs `[p]help <command>`.

These can be defined under your cog's `__init__` function like so:

```python
def __init__(self, liara):
    self.liara = liara
    self.help_set = {
        'group': 'Fun',
        'image': 'https://i.imgur.com/HJcDIhX.png'
    }
```

#### `permissions.py` and `utils/p.py`
This lets you create a per-channel permissions system, where commands only work in specific channels. Cogs can define custom channel types to add commands to, and server admins can add channels to those channel types so the commands can run in those channels.

To limit a command to a specific channel type, you use the `p.staff_or_channel_type()` decorator on the command's function:

```python
@commands.command()
@p.staff_or_channel_type('shitposting')
async def ping(self, ctx):
    """Pong."""
    await ctx.send('Pong.')
```

And to add a channel to a channel type you run `[p]p add channeltype #channel`. For example: `[p]p add shitposting #meme-central`

Channel types are automatically defined when your cog is loaded.

Run `[p]help p` for more info on how to use the permissions cog.

#### `utils/config.py`
This utility allows you to read external JSON files stored in the `/config` folder (can be changed with the `CONFIG` variable in the `.env` file). This allows you to set certain things like emotes or help text that's too long for the `.env` file in an external file instead of hard coded in the source code. Do note it is still recommended to use the `.env` file for certain things, such as API keys.

To load up a config file, import the config utility into your cog and use something like `c = config.Load('file')` where `file` is the file name *without* the extension. You can then either use `c.get('key')` to get a key's value (which falls back to just returning the  key name if it fails) or use `c.dict` to get the full config file as a Dictionary.

Emotes in `emotes.json`, for example, can be added/changed/removed using the `emotes.json` file, and then imported and used in your Cog as follows:

```python
from discord.ext import commands
from cogs.bot.utils import config

emotes = config.Load('emotes')
check = emotes.get('check')

class CogName(commands.Cog):
    def __init__(self, liara):
        self.liara = liara
    
    @commands.command()
    async def yeet(self, ctx):
        await ctx.send(f'{check} Yeet.')

def setup(liara):
    liara.add_cog(CogName(liara))
```

The config utility is used in the [help](/bot/help.py) and [manage](/bot/manage.py) cogs if you'd like to see it in action.

## Maintainers
- lolPants (https://www.jackbaron.com)
- DerpyChap (https://derpychap.co.uk)
