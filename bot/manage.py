import aiohttp
import discord
import asyncio
from discord.ext import commands
from cogs.utils import checks
from cogs.utils.storage import RedisCollection
from cogs.bot.utils import config
from os import listdir
from os.path import isfile, join

emotes = config.Load('emotes')
check = emotes.get('check')
xmark = emotes.get('xmark')
loading = emotes.get('loading')
empty = emotes.get('empty')


class CogNotFoundError(Exception):
    pass


class Manager(commands.Cog):
    """Manage custom bot cogs."""

    def __init__(self, liara):
        self.liara = liara
        self.help_set = {
            'group': 'Core',
            'image': 'https://i.imgur.com/OBoi30q.png'
        }
        self.core = liara.get_cog('Core')
        self.settings = RedisCollection(self.liara.redis, 'settings')
        self.get_traceback = self.core.get_traceback
        # Stuff used for the reload all thing
        self.reload_progress = {
            'cog': None,
            'number': None,
            'checks': '',
            'failed': []
        }

    async def load_cog(self, cog):
        await self.core.load_cog(cog)
        self.liara.dispatch('cog_load', cog)

    async def unload_cog(self, cog, perm: bool = True):
        if cog in list(self.liara.extensions):
            self.liara.unload_extension(cog)
            if perm:
                cogs = await self.settings.get('cogs')
                cogs.remove(cog)
                await self.settings.set('cogs', cogs)
            self.liara.dispatch('cog_unload', cog)
        else:
            raise CogNotFoundError('Cog not found')

    @commands.command('load', aliases=['bl', 'l'])
    @checks.is_owner()
    async def botload(self, ctx, name: str):
        """Loads a cog from the `cogs/bot` folder."""
        name = 'cogs.bot.{}'.format(name)
        await self.load(ctx, name)

    @commands.command('unload', aliases=['bu', 'u'])
    @checks.is_owner()
    async def botunload(self, ctx, name: str):
        """Unloads a cog from the `cogs/bot` folder."""
        name = 'cogs.bot.{}'.format(name)
        await self.unload(ctx, name)

    @commands.command('reload', aliases=['br', 'r'])
    @checks.is_owner()
    async def botreload(self, ctx, name: str):
        """Unloads a cog from the `cogs/bot` folder."""
        name = 'cogs.bot.{}'.format(name)
        await self.reload(ctx, name)

    async def checklist(self, checklist: list, length: int):
        prog = ''
        for i in checklist:
            if i:
                prog += check
            else:
                prog += xmark
        unchecked = length - len(checklist)
        prog += empty * unchecked
        return prog

    async def loader(self, cogs: list):
        """Loads all of the cogs in the specified list."""
        progress = []
        failed = []
        length = len(cogs)
        for i, cog in enumerate(cogs):
            c = await self.checklist(progress, length)
            self.reload_progress = {
                'cog': cog,
                'number': i + 1,
                'checks': c,
                'failed': failed
            }
            if cog in list(self.liara.extensions):
                await self.unload_cog(cog, False)
            try:
                await self.load_cog(cog)
                if cog in list(self.liara.extensions):
                    progress.append(True)
                else:
                    progress.append(False)
                    failed.append(cog)
            except:
                progress.append(False)
                failed.append(cog)
            await asyncio.sleep(0.5)
        self.reload_progress = {
            'cog': None,
            'number': None,
            'checks': await self.checklist(progress, length),
            'failed': failed
        }

    @commands.command()
    @checks.is_owner()
    async def loadall(self, ctx):
        """Loads/reloads all cogs in the `cogs/bot` folder."""
        await ctx.trigger_typing()
        path = 'cogs/bot'
        cog_list = [f for f in listdir(path) if isfile(join(path, f))]
        cogs = []
        for cog in cog_list:
            if cog.endswith('.py'):
                cogs.append('cogs.bot.{}'.format(cog.replace('.py', '')))
        if not cogs:
            # This should never happen.
            return await ctx.send(
                'Something seems to be terribly wrong with the Liara installation.'
            )
        message = None
        self.liara.loop.create_task(self.loader(cogs))
        await asyncio.sleep(0.1)
        while self.reload_progress['cog']:
            p = self.reload_progress
            if len(p['checks']) < 1500:
                c = '\n{}'.format(p['checks'])
            else:
                c = ''
            if not message:
                message = await ctx.send(
                    '{} **{}/{}** - Loading `{}`...{}'.format(
                        loading, p['number'], len(cogs), p['cog'], c))
            else:
                await message.edit(
                    content='{} **{}/{}** - Loading `{}`...{}'.format(
                        loading, p['number'], len(cogs), p['cog'], c))
            await asyncio.sleep(1)
        if not self.reload_progress['failed']:
            await message.edit(
                content='{} All cogs loaded successfully.'.format(check))
        else:
            await message.edit(
                content='{} The following cogs failed to load:\n\n{}'.format(
                    xmark, '\n'.join([
                        '`{}`'.format(x) for x in self.reload_progress['failed']
                    ])))
        self.reload_progress = {
            'cog': None,
            'number': None,
            'checks': '',
            'failed': []
        }

    async def load(self, ctx, name: str):
        if name in self.liara.extensions:
            return await self.reload(ctx, name)

        try:
            await self.load_cog(name)
            await ctx.send('{} `{}` loaded sucessfully.'.format(check, name))
        except ModuleNotFoundError:
            name = 'cogs.{}'.format(name)
            try:
                await self.load_cog(name)
                await ctx.send('{} `{}` loaded sucessfully.'.format(
                    check, name))
            except Exception as e:
                await ctx.send(
                    '{} Unable to load; the cog caused a `{}`:\n```py\n{}\n```'.
                    format(xmark,
                           type(e).__name__, self.get_traceback(e)))
        except Exception as e:
            await ctx.send(
                '{} Unable to load; the cog caused a `{}`:\n```py\n{}\n```'.
                format(xmark,
                       type(e).__name__, self.get_traceback(e)))

    async def unload(self, ctx, name: str):
        if name == 'core':
            await ctx.send(
                '{} Sorry, I can\'t let you do that. '
                'If you want to install a custom loader, look into the documentation (that doesn\'t exist yet lol).'
                .format(xmark))
            return
        if name in list(self.liara.extensions):
            await self.unload_cog(name)
            await ctx.send('{} `{}` unloaded successfully.'.format(check, name))
        else:
            await ctx.send(
                '{} Unable to unload; that cog isn\'t loaded.'.format(xmark))

    async def reload(self, ctx, name: str):
        if name == 'core':
            await self.liara.run_on_shard(
                None if self.liara.shard_id is None else 'all',
                self.liara.loop.create_task(self.core.reload_self()))
            await ctx.send(
                'Command dispatched, reloading core on all shards now.')
            return
        if name in list(self.liara.extensions):
            msg = await ctx.send('{} `{}` reloading...'.format(loading, name))
            await self.unload_cog(name, False)
            try:
                await self.load_cog(name)
            except:
                return await msg.edit(
                    content='{} `{}` reloaded unsuccessfully.'.format(
                        xmark, name))
            if name in list(self.liara.extensions):
                await msg.edit(content='{} `{}` reloaded successfully.'.format(
                    check, name))
            else:
                await msg.edit(
                    content=
                    '{} `{}` reloaded unsuccessfully on a non-main shard. Check your shard\'s logs for '
                    'more details. The cog has not been loaded on the main shard.'
                    .format(xmark, name))
        else:
            await self.load(ctx, name)

    @commands.command('oldload')
    @checks.is_owner()
    async def cmd_load(self, ctx, name: str):
        """Loads a cog.

        **name:** The name of the cog to load.
        """
        await self.load(ctx, name)

    @commands.command('oldunload')
    @checks.is_owner()
    async def cmd_unload(self, ctx, name: str):
        """Unloads a cog.

        **name:** The name of the cog to unload.
        """
        await self.unload(ctx, name)

    @commands.command('oldreload')
    @checks.is_owner()
    async def cmd_reload(self, ctx, name: str):
        """Reloads a cog.

        **name:** The name of the cog to reload.
        """
        await self.reload(ctx, name)


def setup(liara):
    liara.remove_command('load')
    liara.remove_command('unload')
    liara.remove_command('reload')
    liara.add_cog(Manager(liara))
