import discord
import asyncio
import random
from mattophobia_says import MattSays  # This mostly exists for my own amusement as my code falls apart
from discord.ext import commands
from discord.ext.commands import errors as commands_errors
from cogs.utils import checks
from cogs.bot.utils import help, p, config


class errors(commands.Cog):
    """Custom error handler."""

    def __init__(self, liara):
        self.liara = liara
        self.core = liara.get_cog('Core')
        self.core.informative_errors = False  # I can't believe this worked
        self.get_traceback = self.core.get_traceback
        self.matt = MattSays()
        self.config = config.Load('errors')
        self.tracker = self.config.get('issue_tracker', True)

    @commands.Cog.listener()
    async def on_command_error(self, ctx, exception):
        # Code modified from Liara's own error handler, with extra profanities ;)
        try:
            author = ctx.author
            if isinstance(exception, commands_errors.CommandInvokeError):
                exception = exception.original

                error = '`{}` in command `{}`: ```py\n{}\n```'.format(
                    type(exception).__name__, ctx.command.qualified_name,
                    self.get_traceback(exception))
                self.liara.logger.error(error)
                if checks.owner_check(ctx):
                    mattstr = self.matt.generate()
                    await ctx.send(f'{mattstr}\n\n{error}')
                else:
                    msg = '`An error occured while running that command.`'
                    if self.tracker:
                        msg += f'\n\nSeeing this frequently? Submit a bug report here: <{self.tracker}>'
                    await ctx.send(msg)
            if isinstance(exception, discord.Forbidden):
                return await ctx.send(
                    '`I don\'t have permission to perform the action you requested.`'
                )
            if isinstance(exception, commands_errors.CommandNotFound):
                return  # be nice to other bots
            if isinstance(exception, commands_errors.MissingRequiredArgument):
                return await help.sendHelp(ctx)
            if isinstance(exception, commands_errors.BadArgument):
                if checks.owner_check(ctx):
                    await ctx.send(
                        f'{mattstr}\n\n`Bad argument speficied`',
                        embed=await help.ctxEmbed(ctx))
                else:
                    await ctx.send(
                        '`Bad argument specified`',
                        embed=await help.ctxEmbed(ctx))
            if isinstance(exception, commands_errors.DisabledCommand):
                return
            if isinstance(exception, commands_errors.NoPrivateMessage):
                return
            if isinstance(exception, commands.CommandOnCooldown):
                await ctx.send(
                    f'{author.mention} Hold your horses! Try waiting a bit before using that command again.'
                )
            if isinstance(exception, p.InvalidChannelType):
                msg = f'{author.mention} You can\'t run that command here.'
                perms = self.liara.get_cog('permissions')
                if p:
                    approved = []
                    channel_types = await perms.db.get('channel_types')
                    for t in exception.args[1]:
                        t = channel_types.get(t)
                        if t:
                            channels = t.get(ctx.guild.id, [])
                            for channel in channels:
                                c = self.liara.get_channel(channel)
                                if c:
                                    approved.append(c)
                    if approved:
                        matches = []
                        for channel in ctx.guild.text_channels:
                            if channel in approved:
                                user_perms = channel.permissions_for(author)
                                if user_perms.read_messages and user_perms.send_messages:
                                    matches.append(channel)
                        if matches:
                            channel = random.choice(matches)
                            msg += f' Try using it in {channel.mention} instead.'
                try:
                    reply = await ctx.send(msg)
                    await asyncio.sleep(5)
                    await reply.delete()
                    await ctx.message.delete()
                except:
                    pass
            if isinstance(exception, p.InvalidChannelId):
                msg = f'{author.mention} You can\'t run that command here.'
                channel_ids = exception.args[1]
                channels = [
                    self.liara.get_channel(c)
                    for c in channel_ids
                    if self.liara.get_channel(c) is not None
                ]
                matches = []
                for channel in ctx.guild.text_channels:
                    if channel in channels:
                        user_perms = channel.permissions_for(author)
                        if user_perms.read_messages and user_perms.send_messages:
                            matches.append(channel)
                if matches:
                    channel = random.choice(matches)
                    msg += f' Try using it in {channel.mention} instead.'
                try:
                    reply = await ctx.send(msg)
                    await asyncio.sleep(5)
                    await reply.delete()
                    await ctx.message.delete()
                except:
                    pass
            if isinstance(exception, commands_errors.CheckFailure):
                return
        except discord.HTTPException:
            pass


def setup(liara):
    liara.add_cog(errors(liara))
